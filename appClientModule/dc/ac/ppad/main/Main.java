package dc.ac.ppad.main;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.spi.InitialContextFactory;

import dc.ac.ppad.beans.MyStatefullSessionRemote;
import dc.ac.ppad.beans.MyStatelessSessionRemote;


public class Main {

	public static void main(String[] args) throws Exception {

		Hashtable<String, Object> env = new Hashtable<String, Object>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, InitialContextFactory.class.getName());
		env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, "http-remoting://127.0.0.1:8080"));
		env.put("jboss.naming.client.ejb.context", true);

		// env.put(Context.SECURITY_PRINCIPAL, "user");
		// env.put(Context.SECURITY_CREDENTIALS, "passwd");
		InitialContext ic = new InitialContext(env);
		testStateless(ic);
		testStateful(ic);
	}

	private static void testStateless(InitialContext ic) throws Exception {

		System.out.println("[Stateless remote service interaction begin]");
		
		MyStatelessSessionRemote statelessRemote = (MyStatelessSessionRemote) ic
				.lookup("/EJB Server/MyStatelessSession!ejb.MyStatelessSessionRemote");
		System.out.println("Remote service acquired successfuly: " + statelessRemote);
		
		for (int i = 0; i < 2; ++i) {

			System.out.println("About to invoke remote service...");
			String result = statelessRemote.runEcho("hello");
			System.out.println("Result from remote service: " + result);
		}

		System.out.println("[Stateless remote service interaction end]");
	}

	private static void testStateful(InitialContext ic) throws Exception {
		System.out.println("[Stateful remote service interaction begin]");
		
		MyStatefullSessionRemote statefulRemote = (MyStatefullSessionRemote) ic
				.lookup("/TestEJB/MyStatefulSession!ejb.MyStatefulSessionRemote");
		
		System.out.println("Remote service acquired successfuly: " + statefulRemote);
		for (int i = 0; i < 5; ++i) {
		
			System.out.println("About to invoke remote service...");
			int result = statefulRemote.getAndIncrement();
			System.out.println("Result from remote service: " + result);
		}
		
		System.out.println("[Stateful remote service interaction end]");
	}
}